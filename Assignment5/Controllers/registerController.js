var express = require('express');
var router = express.Router();

var Post = require('../Models/mongo/register.js');

//POST URL: /register
router.post('/', function(req, res) {
	console.log("register");
    var new_post = new Post({
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    });
	console.log(new_post);
    new_post.save(function(err) {
        if (err) console.log(err);
        console.log('Registerd successfully!');
    });
    res.render('register/index', { title: 'Register', message: 'Welcome to the register page.'});
});


// GET URL /register/
router.get('/', function(req, res) {
    //Find All Post
    Post.find(function(err, post) {
        res.render('register/index', { title: 'Register', message: 'Welcome to the register page.', data:post});
    });
});
module.exports = router;