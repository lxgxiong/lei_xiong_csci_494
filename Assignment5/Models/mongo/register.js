// The register model?
var mongoose = require('mongoose')
    ,Schema = mongoose.Schema
    ,ObjectId = Schema.ObjectId;
var registerSchema = new Schema({
    username: String,
    password: String,
    email:String
});

module.exports = mongoose.model('Register', registerSchema);